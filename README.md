# File 

Put your files in archivist_tables (overwriting what is there).

template: look don't touch ;-)


## Archivist instance

After it completes the pipeline, please view it at:
[closer-build](https://closer-build.herokuapp.com/)


## Some tests on your tables

| Test                             | Description                                                                   |
| :---                             | :----                                                                         |
| test_same_type                   | All input files should have the same suffix                                   |
| test_names_subset                | All input files should have valid names                                       |
| test_headers_match_template      | All input files should have valid headers                                     |
| test_unique_label                | All input files (except codelist) should not have duplicated lables           |
| test_special_characters          | All Lable columns should not have non-standard characters ["(", ")", " "]     |
| test_code_response               | Same codelist should have same min/max response                               |
| test_qi_response                 | All question items should contain response domain                             |
| test_question_literal            | All questions should contain literal                                          |
| test_statement_literal           | All statements should contain literal                                         |
| test_parent_label                | All child should have parent lable                                            |
| test_parent_type                 | All child should have parent type                                             |
| test_same_question_same_position | Same question should have same position                                       |


## wiki links
- [Transfer questionnaire information to CSV tables.](https://wiki.ucl.ac.uk/pages/viewpage.action?pageId=151260456)

